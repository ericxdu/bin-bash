--Bin Bash - a cooperative jumping game for up to two players
--Copyright 2021 Eric Duhamel

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.

--You should have received a copy of the GNU General Public License
--along with this program.  If not, see <https://www.gnu.org/licenses/>.
function lutro.load()
  LEFT, TOP, RIGHT, BOTTOM = 0, 0, 320, 240
  FONT = lutro.graphics.newImageFont("font00.png",
    ' ! #$%& ()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_')
  FRAMES, OBJECTS, PLAYERS = load_level(0)
  BACKDROP = draw_backdrop(FRAMES)
end

function lutro.update(dt)
  for i,obj in ipairs(OBJECTS) do
    obj_accelerate(obj, dt)
    obj_move(obj, dt)
    local wrap = obj_wrap(obj, LEFT, TOP, RIGHT, BOTTOM)
    if wrap and obj.looper and obj.y > 200 then
      obj.x = wrap == LEFT and LEFT-obj.w or RIGHT
      obj.y, obj.dx = 32, -obj.dx
    end
    for _,fra in ipairs(FRAMES) do
      if obj.reflect then
        obj_pongout(obj, fra.x, fra.y, fra.x+fra.w, fra.y+fra.h)
      else
        obj_pushout(obj, fra.x, fra.y, fra.x+fra.w, fra.y+fra.h)
      end
    end
    for j=0,#OBJECTS-1,1 do local tar = OBJECTS[((i+j)%#OBJECTS)+1]
      if obj_is_touching(obj, tar) then
        if obj.bug and (tar.bug or tar.player) then
          if tar.y <= obj.y then obj_turnback(tar, obj) end
        end
        if obj.dy > 0 and obj.stomper then obj_stomp(obj, tar) end
        if (obj.spiker or obj.burner) and tar.tender then
          obj_knockback(obj, tar)
          BACKDROP = draw_backdrop(FRAMES)
        end
      end
    end
    if obj.drag then obj_decelerate(obj, obj.drag*dt) end
    animate_char(obj)
  end
end

function obj_knockback(obj, tar) if not tar.invuln then
  tar.invuln, tar.knocked = true, true
  if obj.attack then obj.attack:play() end
  if tar.coins then
    if tar.coins < 1 then tar.falloff = true
    else tar.coins = tar.coins-1 end
  end
  if obj.x < tar.x then
    tar.x, tar.dx, tar.dy = obj.x+obj.w, 150, -200
  elseif obj.x > tar.x then
    tar.x, tar.dx, tar.dy = obj.x-tar.w, -150, -200
  end
end end

function obj_stomp(obj, tar)
  if tar.stompable and not tar.stomped and obj.y < tar.y-(tar.h/2)
     and not obj.knocked then
    tar.dx, obj.dy = 0, -120
    tar.falloff, tar.stomped, tar.spiker = true, true, false
    tar.stomp:play()
  end
end

function obj_turnback(obj, tar)
  if obj.x < tar.x then
    tar.x, tar.dx = obj.x+obj.w, math.abs(tar.dx)
  elseif obj.x > tar.x then
    tar.x, tar.dx = obj.x-tar.w, -math.abs(tar.dx)
  end
end

function lutro.draw()
  lutro.graphics.scale(2)
  lutro.graphics.draw(BACKDROP, 0, 0)
  for i,obj in ipairs(OBJECTS) do obj_draw(obj) end
end

function animate_char(obj)
  local now = lutro.timer.getTime()
  if obj.qy then
    obj.qua:setViewport(math.floor(now*15%obj.f)*16, obj.qy, 16, 16)
  elseif obj.stomped then  -- when a bug gets stomped
    obj.qua:setViewport(80, 64, obj.ih, obj.iw)
  elseif obj.knocked then  -- when character is knocked
    local y = obj.face > 0 and 16 or 0
    obj.qua:setViewport(64, y, obj.ih, obj.iw)
  elseif obj.invuln then  -- when character is invulnerable
    local y = obj.face > 0 and 64 or 80
    obj.qua:setViewport(32+(math.floor(now*15%2)*32), y, obj.ih, obj.iw)
  elseif obj.dy < 0 then  -- jumping
    local y = obj.face > 0 and 64 or 80
    obj.qua:setViewport(0, y, obj.ih, obj.iw)
  elseif obj.dy > 15 then  -- falling
    local y = obj.face > 0 and 64 or 80
    obj.qua:setViewport(16, y, obj.ih, obj.iw)
  elseif obj.dx > 0 then  -- running right
    obj.qua:setViewport(math.floor(now*10%6)*16, 32, obj.ih, obj.iw)
  elseif obj.dx < 0 then  -- running left
    obj.qua:setViewport(math.floor(now*10%6)*16, 48, obj.ih, obj.iw)
  else  -- idle
    local x, y = math.floor(now*5%4), obj.face > 0 and 0 or 16
    obj.qua:setViewport(x*16, y, obj.ih, obj.iw)
  end
end

function lutro.joystickpressed(n, b)
  if PLAYERS and #PLAYERS > n then local obj = PLAYERS[n+1]
    if b == 0 and obj.landed then
      obj.xaccel, obj.adown, obj.dy = 275, 325, -200
      obj.jump:play()
    elseif b == 6 and not obj.knocked then
      obj.aleft, obj.invuln = obj.xaccel, false
    elseif b == 7 and not obj.knocked then
      obj.aright, obj.invuln = obj.xaccel, false
    end
  end
end

function lutro.joystickreleased(n, b)
  if PLAYERS and #PLAYERS > n then local obj = PLAYERS[n+1]
    if b == 0 then obj.xaccel, obj.adown = 300, 650
    elseif b == 6 then obj.aleft = false
    elseif b == 7 then obj.aright = false end
  end
end

function obj_accelerate(obj, dt)
  if obj.aleft and obj.dx > -75 then
    obj.face, obj.dx = -1, obj.dx-(obj.aleft*dt)
  elseif obj.aright and obj.dx < 75 then
    obj.face, obj.dx = 1, obj.dx+(obj.aright*dt)
  end
  if obj.adown and obj.dy < 750 then obj.dy = obj.dy+(obj.adown*dt) end
end

function draw_backdrop(frames)
  local canvas = lutro.graphics.newCanvas(320, 240)
  local bgimg = lutro.graphics.newImage('back02.png')
  if lutro.graphics.reset then lutro.graphics.reset() end
  lutro.graphics.setCanvas(canvas)
  lutro.graphics.draw(bgimg)
  for _,fra in ipairs(frames) do
    if fra.img and fra.qua then
      for i=0,fra.w,16 do
        lutro.graphics.draw(fra.img, fra.qua, fra.x+i, fra.y)
      end
    end
--    lutro.graphics.rectangle('line', fra.x, fra.y, fra.w, fra.h)
  end
  lutro.graphics.setColor(0, 0, 0)
  lutro.graphics.rectangle('fill', 0, 232, 100, 8)
  lutro.graphics.rectangle('fill', 220, 232, 100, 8)
  lutro.graphics.setFont(FONT)
  for i,obj in ipairs(PLAYERS) do
    local x, y = i == 1 and 0 or 220, 232
    lutro.graphics.setColor(1, 1, 1)
    lutro.graphics.print(obj.name .. '-', x, y)
    x = x+(FONT:getWidth(obj.name .. '-'))
    for j=0,4,1 do
      local symbol = obj.coins > j and '$' or '.'
      lutro.graphics.print(symbol, x+(j*8), y)
    end
  end
  lutro.graphics.setCanvas()
  return canvas
end

function load_level()
  local tiles = lutro.graphics.newImage('page00.png')
  local glitch = lutro.graphics.newImage('ball01.png')
  local RedBug = lutro.graphics.newImage('char02.png')
  local Daemon = lutro.graphics.newImage('char04.png')
  local Mallo = lutro.graphics.newImage('char05.png')
  local bluesteel = lutro.graphics.newQuad(96, 0, 16, 16, 128, 128)
  local greencirc = lutro.graphics.newQuad(0, 48, 16, 16, 128, 128)
  local greenmini = lutro.graphics.newQuad(16, 48, 16, 8, 128, 128)
  local frames = {
    {qua = bluesteel, img = tiles, x = -16, y = 48, w = 127, h = 16},
    {qua = bluesteel, img = tiles, x = 192, y = 48, w = 127, h = 16},
    {qua = bluesteel, img = tiles, x = 48, y = 104, w = 207, h = 16},
    {qua = bluesteel, img = tiles, x = -16, y = 160, w = 111, h = 16},
    {qua = bluesteel, img = tiles, x = 208, y = 160, w = 111, h = 16},
    {qua = greenmini, img = tiles, x = -17, y = 216, w = 351, h = 16},
    {qua = greencirc, img = tiles, x = -17, y = 224, w = 351, h = 16}}
  local objects = {
    new_bug(RedBug, 100, 75),
    new_bug(RedBug, 300, 40),
    new_bug(RedBug, 200, 150),
    new_ball(glitch, 30, 100),
    new_player(Daemon, 50, 200, 'DAEMON'),
    new_player(Mallo, 250, 200, 'MALLO')}
  return frames, objects, {objects[5], objects[6]}
end

function new_ball(image, x, y)  -- a glitch in the system
  local dx = x > 160 and -25 or 25
  return {img = image, qua = lutro.graphics.newQuad(0, 0, 16, 16, 128, 64),
    iw = 16, ih = 16, ox = -1, oy = -1, f = 8, qy = 48,
    ball = true, burner = true, reflect = true,
    attack = lutro.audio.newSource('Hit_03.wav', 'static'),
    reflect = lutro.audio.newSource('Menu_Navigate_03.wav', 'static'),
    x = x, y = y, w = 14, h = 14, dx = dx, dy = 25}
end

function new_bug(image, x, y)  -- a bug in the system
  local dx, face = x > 160 and -25 or 25, x > 160 and 1 or -1
  local face = dx > 0 and 1 or -1
  return {img = image, qua = lutro.graphics.newQuad(0, 0, 16, 16, 96, 128),
    iw = 16, ih = 16, ox = -2, oy = -4, face = face,
    bug = true, looper = true, spiker = true, stompable = true, tpiker = false,
    attack = lutro.audio.newSource('Pickup_00.wav', 'static'),
    fall = lutro.audio.newSource('Pukcip_03.wav', 'static'),
    stomp = lutro.audio.newSource('Shoot_01.wav', 'static'),
    x = x, y = y, w = 12, h = 12, dx = dx, dy = 25, adown = 500}
end

function new_coin(image, x, y)  -- free memory
  local dx, face = x > 160 and -25 or 25, x > 160 and 1 or -1
  return {img = image, qua = lutro.graphics.newQuad(0, 0, 16, 16, 128, 16),
    iw = 16, ih = 16, ox = -5, oy = 0, qy = 0, f = 8, face = face,
    collectable = true, looper = true,
    x = x, y = y, w = 6, h = 16, dx = dx, dy = 0, adown = 500}
end

function new_player(image, x, y, name)
  return {img = image, qua = lutro.graphics.newQuad(0, 0, 16, 16, 96, 128),
    iw = 16, ih = 16, ox = -2, oy = 0,  -- image properties
    x = x, y = y, w = 11, h = 16,  -- physical properties
    dx = 0, dy = 0, coins = 3, face = 1, name = name, player = true,
    fall = lutro.audio.newSource('Pukcip_03.wav', 'static'),
    jump = lutro.audio.newSource('Jump_03.wav', 'static'),
    xaccel = 300, drag = 200, adown = 650, stomper = true, tender = true}
end

function obj_bind(obj, left, top, right, bottom)
  local bind = false
  if right and obj.x >= right-obj.w then
    bind, obj.x, obj.dx = right, right-obj.w, obj.dx > 0 and 0 or obj.dx
  elseif left and obj.x <= left then
    bind, obj.x, obj.dx = left, left, obj.dx < 0 and 0 or obj.dx
  end
  if obj.y ~= 0 then obj.landed = false end
  if bottom and obj.y > bottom-obj.h and not obj.falloff then
    bind, obj.y, obj.dy = bottom, bottom-obj.h, obj.dy > 0 and 0 or obj.dy
    obj.knocked, obj.landed = false, true
  elseif top and obj.y < top then
    bind, obj.y, obj.dy = top, top, obj.dy < 0 and 0 or obj.dy
  end
  return bind
end

function obj_decelerate(obj, drag)
  if obj.dx > drag then obj.dx = obj.dx - drag
  elseif obj.dx < -drag then obj.dx = obj.dx + drag
  else obj.dx = 0 end
end

function obj_draw(obj)
--  lutro.graphics.rectangle('line', obj.x, obj.y, obj.w, obj.h)
  if INVERT then ox, oy = -obj.ox, -obj.oy else ox, oy = obj.ox, obj.oy end
  lutro.graphics.draw(obj.img, obj.qua, obj.x, obj.y, 0, 1, 1, ox, oy)
end

function obj_is_touching(ob1, ob2)
  if ob1.x+ob1.w > ob2.x and ob1.x < ob2.x+ob2.w
     and ob1.y+ob1.h > ob2.y and ob1.y < ob2.y+ob2.h then return true
  else return false end
end

function obj_move(obj, dt)
  obj.x, obj.y = obj.x + (obj.dx*dt), obj.y + (obj.dy*dt)
  if obj.dx ~= 0 then obj.face = obj.dx > 0 and 1 or -1 end
end

function obj_collide(obj, left, top, right, bottom)
  local ax, ay = right-obj.x, bottom-obj.y
  local bx, by = (obj.x+obj.w)-left, (obj.y+obj.h)-top
  local cx, cy, fx, fy = 0, 0, 0, 0
  if ax > 0 and ay > 0 and bx > 0 and by > 0 then
    if ax < bx then cx = ax else cx = -bx end
    if ay < by then cy = ay else cy = -by end
    if math.abs(cx) < math.abs(cy) then fx = cx else fy = cy end
  end
  return fx, fy
end

function obj_pongout(obj, left, top, right, bottom)
  local fx, fy = obj_collide(obj, left, top, right, bottom)
  if fx ~= 0 then obj.reflect:play()
    if fx > 0 then obj.x, obj.dx = right, math.abs(obj.dx)
    elseif fx < 0 then obj.x, obj.dx = left-obj.w, -math.abs(obj.dx) end
  end
  if fy ~= 0 then obj.reflect:play()
    if fy > 0 then obj.y, obj.dy = bottom, math.abs(obj.dy)
    elseif fy < 0 then obj.y, obj.dy = top-obj.h, -math.abs(obj.dy) end
  end
end

function obj_pushout(obj, left, top, right, bottom)
  local fx, fy = obj_collide(obj, left, top, right, bottom)
  if fx ~= 0 then obj.dx = 0
    if fx > 0 then obj.x = right else obj.x = left-obj.w end
  end
  if obj.dy ~= 0 then obj.landed = false end
  if fy ~= 0 then obj.dy = 0
    if fy > 0 then obj.y = bottom else
      obj.knocked, obj.landed, obj.y = false, true, top-obj.h
    end
  end 
end

function obj_wrap(obj, left, top, right, bottom)
  if right and obj.x > right then obj.x = left-obj.w return right
  elseif left and obj.x+obj.w < left then obj.x = right return left end
  if bottom and obj.y > bottom then 
    if obj.fall and obj.dy > 0 then obj.fall:play()
      obj.dy, obj.adown = 0, 0
    else obj.y = top-obj.h return bottom end
  elseif top and obj.y+obj.h < top then obj.y = bottom return top end
end
