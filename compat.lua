function love.keypressed(key)
  if key == 'escape' then love.event.quit()
  elseif key == 's' then lutro.joystickpressed(0, 0)
  elseif key == 'a' then lutro.joystickpressed(0, 1)
  elseif key == 'left' then lutro.joystickpressed(0, 6)
  elseif key == 'right' then lutro.joystickpressed(0, 7)
  elseif key == 'kp0' then lutro.joystickpressed(1, 0)
  elseif key == 'kp1' then lutro.joystickpressed(1, 1)
  elseif key == 'kp4' then lutro.joystickpressed(1, 6)
  elseif key == 'kp6' then lutro.joystickpressed(1, 7)
  end
end

function love.keyreleased(key)
  if key == 's' then lutro.joystickreleased(0, 0)
  elseif key == 'a' then lutro.joystickreleased(0, 1)
  elseif key == 'left' then lutro.joystickreleased(0, 6)
  elseif key == 'right' then lutro.joystickreleased(0, 7)
  elseif key == 'kp0' then lutro.joystickreleased(1, 0)
  elseif key == 'kp1' then lutro.joystickreleased(1, 1)
  elseif key == 'kp4' then lutro.joystickreleased(1, 6)
  elseif key == 'kp6' then lutro.joystickreleased(1, 7)
  end
end
