function love.conf(t)
  t.version = '11.1'
  if t.window then
    t.window.title = 'Bin Bash'
    t.window.width = 640
    t.window.height = 480
  end
--  if lutro then t.modules.keyboard = true end
end

-- do some compatibility stuff for LÖVE
if not lutro then
  lutro = love
  INVERT = true
  require'compat'
end
